// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування - це наслідування класом-нащадком методів з прототипів батьківських класів. Наприклад, будь-який обьєкт є класом нащадком глобального класу Object, тому всі методі з прототипу цього класу доступні всім об'єктам

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// тому що клас нащадок доповнює батьківський клас, відповідно для того щоб цого доповнити його треба створити, метод super() створює об'єкт з батьківського класу, потім ми його доповнюємо.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
  set lang(value) {
    this._lang = value;
  }
  get lang() {
    return this._lang;
  }
}

const programmer1 = new Programmer("Polad", 22, "800", ["JS", "HTML", "CSS"]);
console.log(programmer1);
const programmer2 = new Programmer("Dmytro", 24, "1000", "C++");
console.log(programmer2);
const programmer3 = new Programmer("Iryna", 22, "1500", ["C#", "Python", "Ruby"]);
console.log(programmer3);
